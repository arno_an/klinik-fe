import React, { Component } from 'react'
import { Route } from "react-router-dom"
// import { history } from 'helpers'
// import toastr from 'toastr'
import Sidebar from './admin/layout/Sidebar'
import Navbar from './admin/layout/Navbar'
import Dashboard from './admin/Dashboard'
import Pasien from './admin/pasien'
import Kunjungan from './admin/kunjungan'
import Layanan from './admin/layanan'
import Testimoni from './admin/testimoni'
import Pengguna from './admin/pengguna'
import Transaksi from './admin/transaksi'
import Profile from './admin/profile'

class Admin extends Component{
  constructor(props) {
    super(props)
  
    this.state = { 
      showLogin: false
    }
  }

  componentDidMount() {
    // if(localStorage.klinik_id){
    // }else{
    //   localStorage.setItem('error', "Anda tidak memiliki akses!")
    //   window.location.href = '/public'

    //   toastr.error('Anda tidak memiliki akses!') 
    //   return null
    // }
  }
  
  handleLoginClick = () => {
    this.setState({showLogin: !this.state.showLogin})
  }

  render(){
    const { location } = this.props
    return (
      <div className="wrapper d-flex bg-light">
        <Sidebar />
        <div className="content-wrapper w-100">
          <Navbar location={location.pathname}/>

          <Route path="/admin/" exact component={Dashboard} />
          <Route path="/admin/pasien" exact component={Pasien} />
          <Route path="/admin/kunjungan" exact component={Kunjungan} />
          <Route path="/admin/layanan" exact component={Layanan} />
          <Route path="/admin/testimoni" exact component={Testimoni} />
          <Route path="/admin/pengguna" exact component={Pengguna} />
          <Route path="/admin/transaksi" exact component={Transaksi} />

          <Route path="/admin/profile" exact component={Profile} />
        </div>
      </div>
    )
  }
}

export default Admin
