import React, { Component } from 'react'
import { Router, Route, Redirect, Switch } from "react-router-dom"
import Admin from './Admin'
import Public from './Public'
import { history } from 'helpers'

export class AppRouter extends Component {
  render() {
    return (
      <Router history={history}>
        <Switch>

          <Route path="/public" component={Public} />

          <PrivateRoute path="/admin" >
            <Route path="/admin" component={Admin} />
          </PrivateRoute>
            
          <Redirect from="/" to="/public" />
        </Switch>
      </Router>
    )
  }
}

function PrivateRoute({ children, ...rest }) {
  // console.log(children)
  return (
    <Route
      {...rest}
      render={({ location }) =>
        localStorage.klinik_id ? (
          children
        ) : (
          <Redirect 
            to={{
              pathname: "/public",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

export default AppRouter
