import React, { Component, Fragment } from 'react'
// import { Provider } from 'react-redux';
import { NavLink, Route, Redirect, Switch } from "react-router-dom"
import logo from 'assets/icon/ic-logo.png'
import Home from './public/home'
import Login from './public/login'
import Pricelist from './public/pricelist'
import toastr from "toastr"

function testimoni() {
  toastr.success("testimoni")

  return(
    <div>testimoni</div>
  )
}

class Public extends Component{
  constructor(props) {
    super(props)
  
    this.state = { 
      showLogin: false
    }
  }
  
  componentDidMount() {
  }
  

  handleLoginClick = () => {
    this.setState({showLogin: !this.state.showLogin})
  }

  render(){
    const { showLogin } = this.state
    const { match } = this.props;
    
    return (
      <Fragment>
        <nav className={`navbar navbar-expand-md navbar-dark bg-primary`}>
          <a className="navbar-brand" href="nvabar">
            <span className="ml-2"><img src={logo} alt="logo" className="mr-2"></img> Klinik Bhakti Mulus </span>
          </a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse " id="navbarNavAltMarkup">
            <div className="navbar-nav ml-auto">
              <NavLink to={`${match.path}/home`} className="nav-item nav-link ml-3" activeClassName="active">Beranda <span className="sr-only">(current)</span></NavLink>
              {/* <NavLink to={`${match.path}/testimoni`} className="nav-item nav-link ml-3" activeClassName="active">Testimoni</NavLink> */}
              <NavLink to={`${match.path}/pricelist`} className="nav-item nav-link ml-3" activeClassName="active">Pricelist</NavLink>
              <a href="#login" className="nav-item nav-link ml-3 login" onClick={() => this.handleLoginClick()}>Login</a>
            </div>
          </div>
        </nav>
        
        <Login showLogin={showLogin} handleLoginClick={() => this.handleLoginClick()} />

        <Switch>
          <Route path={`${match.path}/home`} component={Home} />
          <Route path={`${match.path}/testimoni`} component={testimoni} />
          <Route path={`${match.path}/pricelist`} component={Pricelist} />
        </Switch>

        <Redirect from="/public" to="/public/home" />
      </Fragment>
    )
  }
}

export default Public
