import React, { Component } from 'react'
import DashboardAtas from './dashboard/Atas'
import TableKunjungan from './dashboard/Table'
import ChartKunjungan from './dashboard/ChartKunjungan'
import ChartPasien from './dashboard/ChartPasien'

export class Dashboard extends Component {
  render() {
    return (
      <div className="p-3">
        <DashboardAtas />
        <TableKunjungan />
        <hr/>
        <div className="row">
          <div className="col-4 text-center">
            <ChartPasien />
          </div>
          <div className="col-8">
            <ChartKunjungan />
          </div>
        </div>
      </div>
      )
    }
  }
  
  export default Dashboard
