import React, { Component } from 'react'
import { FaUsers, FaCalendarCheck, FaUserPlus, FaCalendarDay } from 'react-icons/fa'
import axios from 'axios'
import toastr from 'toastr'

export class DashboardAtas extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       totalPasien: 0,
       pasienBaru: 0,
       totalKunjungan: 0,
       kunjunganToday: 0,
    }
  }

  componentDidMount() {
    this.getTotalKunjungan()
    this.getTotalKunjunganToday()
    this.getTotalPasien()
    this.getPasienBaru()
  }
  
  
  getTotalPasien = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/statistik/total_pasien`)
      .then( res => {
        this.setState({totalPasien: res.data[0].total_pasien})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }
  
  getPasienBaru = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/statistik/total_pasien_today`)
      .then( res => {
        this.setState({pasienBaru: res.data[0].total_pasien_today})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }
  
  getTotalKunjungan = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/statistik/total_kunjungan`)
      .then( res => {
        this.setState({totalKunjungan: res.data[0].total_kunjungan})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }
  
  getTotalKunjunganToday = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/statistik/total_kunjungan_today`)
      .then( res => {
        this.setState({kunjunganToday: res.data[0].total_kunjungan_today})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  render() {
    const { totalPasien, pasienBaru, totalKunjungan, kunjunganToday } = this.state
    return (
      <div className="row p-3 container-atas">
        <div className="col-3 p-3">
          <div className="border d-flex p-3 bg-white text-primary">
            <FaUsers size={46} className="mr-3"/>
            <div className="d-flex flex-column">
              <span>Total Pasien</span>
              <strong>{totalPasien}</strong>
            </div>
          </div>
        </div>
        <div className="col-3 p-3">
          <div className="border d-flex p-3 bg-white text-primary">
            <FaUserPlus size={46} className="mr-3"/>
            <div className="d-flex flex-column">
              <span>Pasien Baru</span>
              <strong>{pasienBaru}</strong>
            </div>
          </div>
        </div>
        <div className="col-3 p-3">
          <div className="border d-flex p-3 bg-white text-primary">
            <FaCalendarCheck size={46} className="mr-3"/>
            <div className="d-flex flex-column">
              <span>Total Kunjungan</span>
              <strong>{totalKunjungan}</strong>
            </div>
          </div>
        </div>
        <div className="col-3 p-3">
          <div className="border d-flex p-3 bg-white text-primary">
            <FaCalendarDay size={46} className="mr-3"/>
            <div className="d-flex flex-column">
              <span>Kunjungan Hari Ini</span>
              <strong>{kunjunganToday}</strong>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default DashboardAtas
