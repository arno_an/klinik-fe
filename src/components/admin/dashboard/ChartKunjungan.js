import React, { Component, Fragment } from 'react'
import Chart from 'react-apexcharts'
import axios from 'axios'
import toastr from 'toastr'

export class ChartKunjungan extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      options: {
        xaxis: {
          categories: [10, 11, 12, 14, 16, 17, 18, 19]
        },
        title:{
          text: 'Data kunjungan pasien',
          style: {
            fontSize:  '22px',
            color:  '#263238'
          },
        },
        subtitle: {
          text: 'Tahun 2019',
        },
      },
      series: [],
    }
  }
  
  componentDidMount() {
    this.getListKunjungan()
    
  }
  
  getListKunjungan = () => {
    
    axios.get(process.env.REACT_APP_API_HOST + `/statistik/list_kunjungan_bulan`)
      .then( res => {
        let dataDay = []
        let total = []
        res.data.map((data, i) => {
          dataDay.push(parseInt(data.days))
          total.push(parseInt(data.total))
          return true
        })

        this.setState({ 
          series: [{ nama: 'Kunjungan', data: total }],
          options: { xaxis: { categories: dataDay }}
        })
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  render() {
    return (
      <Fragment>
        <Chart options={this.state.options} series={this.state.series} type="line" height={320} />
      </Fragment>
    )
  }
}

export default ChartKunjungan                
