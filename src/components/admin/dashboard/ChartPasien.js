
import React, { Component, Fragment } from 'react'
import Chart from 'react-apexcharts'
import axios from 'axios'
import toastr from 'toastr'

class Donut extends Component {
  constructor(props) {
    super(props);

    this.state = {
      options: {
        title:{
          text: 'Data pasien berdasarkan jenis kelamin',
          style: {
            fontSize:  '22px',
          },
        },
        subtitle: {
          text: 'Tahun 2019',
        },
        legend: {
          show: false
        },
        labels: ['Laki-laki', 'Perempuan']
      },  
      series: [44, 99]
    }

  }

  componentDidMount() {
    this.getData()
    
  }
  

  getData = () => {

    axios.get(process.env.REACT_APP_API_HOST + `/statistik/total_pasien_kelamin`)
      .then( res => {
        this.setState({series: [ parseInt(res.data[0].total), parseInt(res.data[1].total)]})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  render() {

    return (
      <Fragment>
        <Chart options={this.state.options} series={this.state.series} type="donut" height="320px" />
      </Fragment>
    );
  }
}

export default Donut