import React, { Component } from 'react'
import axios from 'axios'
import toastr from 'toastr'

export class TableKunjungan extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      listKunjunganToday: []
    }
  }

  componentDidMount() {
    this.getListKunjungan()
    
  }
  
  
  getListKunjungan = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/statistik/list_kunjungan_today`)
      .then( res => {
        this.setState({listKunjunganToday: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }
  render() {
    const { listKunjunganToday } = this.state
    return (
      <div className="p-3">
        <table className="table table-striped">
          <thead className="bg-primary text-white">
            <tr>
              <th width="20%">Nama Pasien</th>
              <th width="10%">Jam</th>
              <th width="40%">Alamat</th>
              <th width="30%">Keterangan</th>
            </tr>
          </thead>
          <tbody>
          {
            listKunjunganToday.map((data, i) => (
              <tr key={i}>
                <td>{data.pasien}</td>
                <td>{data.jam_kunjungan}</td>
                <td>{data.alamat}</td>
                <td>{data.keterangan}</td>
              </tr>
            ))
          }
          </tbody>
        </table>
      </div>
    )
  }
}

export default TableKunjungan
