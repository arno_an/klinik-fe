import React, { Component, Fragment } from 'react'
import DatePicker from "react-datepicker"
import axios from 'axios'
import toastr from 'axios'
import "react-datepicker/dist/react-datepicker.css"
import moment from 'moment'

export class Cari extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       tanggalKunjungan: moment().valueOf(),
       namaPasien: localStorage.klinik_jenis_user === 'pasien' ? localStorage.klinik_id : '',
       status: '',
       dataPasien: []
    }
    localStorage.getItem('klinik_jenis_user') !== 'pasien' && this.getDataPasien()
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value})
  }

  getDataPasien = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/pengguna?jenis=pasien`)
      .then( res => {
        this.setState({dataPasien: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  handleSubmit = () => {
    this.props.handleCari({
      tanggalKunjungan: moment(this.state.tanggalKunjungan).format('YYYY-MM-DD'), 
      namaPasien: this.state.namaPasien, 
      status: this.state.status, 
      role: localStorage.getItem('klinik_jenis_user')
    })
  }

  handleReset = () => {
    this.setState({
      tanggalKunjungan: '',
      namaPasien: '',
      status: ''
    })
    this.props.handleReset()
  }

  render() {
    const { tanggalKunjungan, namaPasien, dataPasien, status } = this.state
    return (
      <Fragment>
        <div className="row">
          {
            (localStorage.getItem('klinik_jenis_user') !== 'pasien') &&
              <div className="form-group col-sm-3">
                <label>Nama Pasien</label>
                <select name="namaPasien" className="form-control form-control-sm" onChange={this.handleChange} value={namaPasien}>
                  <option value="">-- Pilih Nama Pasien --</option>
                    { dataPasien.map((data, i) => {
                    return (
                      <option key={i} value={data.id_pengguna}>{data.nama}</option>
                    )
                  })}
                </select>
              </div>
          }
          <div className="form-group col-sm-3">
            <label>Tanggal Kunjungan</label>
            <DatePicker className="form-control mr-3 form-control-sm" onChange={date => this.setState({tanggalKunjungan: date }) } selected={tanggalKunjungan} placeholderText="Pilih tanggal kunjungan"/>
          </div>

          <div className="form-group col-sm-3">
            <label>Status</label>
            <select name="status" className="form-control form-control-sm" onChange={this.handleChange} value={status}>
              <option value="">-- Pilih Status --</option>
                <option value="Baru">Baru</option>
                <option value="Disetujui">Disetujui</option>
                <option value="Ditolak">Ditolak</option>
            </select>
          </div>
          
        </div>
        <div className="row">
          <div className="offset-sm-6 col-sm-3 text-right">
            <button className="btn btn-primary px-4 mr-3 btn-sm" onClick={() => this.handleSubmit()}>Cari</button>
            <button className="btn btn-secondary px-4 btn-sm" onClick={() => this.handleReset()}>Reset</button>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default Cari
