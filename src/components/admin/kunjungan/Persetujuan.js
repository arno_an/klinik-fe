import React, { Component, Fragment } from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import axios from 'axios'
import toastr from 'toastr'

export class Persetujuan extends Component {
  handleSubmit = () => {
    let params = {
      id_kunjungan: this.props.idKunjungan,
      status: this.props.isTerima ? 'Disetujui' : 'Ditolak'
    }

    axios.put(process.env.REACT_APP_API_HOST + `/kunjungan/approval`, params)
      .then(res => {
        this.props.getDataKunjungan()
        toastr.success('Data berhasil '+ params.status)
        this.handleModal()
      })
  }

  handleModal = () => {
    this.props.handleShow()
  }

  render() {
    const { nama, jam, tanggal, isOpen, isTerima } = this.props
    return (
      <Fragment>
         <Modal isOpen={isOpen}>
          <ModalHeader>Persetujuan Data Pasien</ModalHeader>
          <ModalBody>
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="col-12">
                  <span>Anda yakin untuk <strong>{isTerima ? 'Menyetujui' : 'Menolak'}</strong> kunjungan <strong>{nama}</strong> pada tanggal {tanggal}, jam {jam}?</span>
                </div>
              </div>
            </form>
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-primary" onClick={() => this.handleSubmit()}>Submit</button>
            <button className="btn btn-secondary" onClick={() => this.handleModal()}>Cancel</button>
          </ModalFooter>
        </Modal>
      </Fragment>
    )
  }
}

export default Persetujuan
