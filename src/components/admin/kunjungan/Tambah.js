import React, { Component, Fragment } from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import axios from 'axios'
import toastr from 'toastr'

class index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      idPasien: localStorage.klinik_jenis_user === 'pasien' ? localStorage.klinik_id : '' ,
      tglKunjungan: '',
      jamKunjungan: '',
      keterangan: '',
      dataPasien: []
    }
  }
  
  componentDidUpdate(prevProps, prevState) {
    if(prevProps.isOpen !== this.props.isOpen && this.props.isOpen === true){
      this.getDataPasien()
    }
  }
  
  componentDidMount() {
    // this.props.isUpdate && this.getData(this.props.idPasien)
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value})
  }

  handleModal = () => {
    this.props.handleShow()
  }

  handleSubmit = () => {
    let params = {
      id_pasien: this.state.idPasien,
      tgl_kunjungan: this.state.tglKunjungan,
      jam_kunjungan: this.state.jamKunjungan,
      keterangan: this.state.keterangan
    }

    console.log(params);
    

    axios.post(process.env.REACT_APP_API_HOST + `/kunjungan`, params)
      .then(res => {
        toastr.success('Data berhasil disimpan')
        this.props.getDataKunjungan()
        this.handleModal()
      })
  }

  getDataPasien = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/pengguna?jenis=pasien`)
      .then( res => {
        this.setState({dataPasien: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  render() {
    const { dataPasien } = this.state 
    const { isOpen } = this.props
    return (
      <Fragment>
         <Modal isOpen={isOpen}>
          <ModalHeader>Tambah Kunjungan</ModalHeader>
          <ModalBody>
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="col-12">
                  {
                    (localStorage.klinik_jenis_user !== 'pasien') &&
                      <div className="form-group">
                        <label>Nama Pasien</label>
                        <select className="form-control" name="idPasien" onChange={this.handleChange}>
                          <option value="">Pilih Pasien</option>
                          {
                            dataPasien.map((data, i) => {
                              return <option key={i} value={data.id_pengguna}>{data.nama}</option>
                            })
                          }
                        </select>
                      </div>
                  }
                  <div className="form-group">
                    <label>Tanggal Kunjungan</label>
                    <input type="date" className="form-control" placeholder="Masukan tanggal Kunjungan" name="tglKunjungan" onChange={this.handleChange}/>
                  </div>
                  <div className="form-group">
                    <label>Jam Kunjungan</label>
                    <input type="text" className="form-control" placeholder="Masukan jam kunjungan" name="jamKunjungan" onChange={this.handleChange} maxLength={5}/>
                  </div>
                  <div className="form-group">
                    <label>Keterangan</label>
                    <input type="text" className="form-control" placeholder="Masukan keterangan" name="keterangan" onChange={this.handleChange} maxLength={50}/>
                  </div>
                </div>
              </div>
            </form>
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-primary" onClick={() => this.handleSubmit()}>Submit</button>
            <button className="btn btn-secondary" onClick={() => this.handleModal()}>Cancel</button>
          </ModalFooter>
        </Modal>
      </Fragment>
    )
  }
}

export default index
