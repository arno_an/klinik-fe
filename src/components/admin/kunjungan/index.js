import React, { Component, Fragment } from 'react'
import Persetujuan from './Persetujuan'
import Tambah from './Tambah'
import axios from 'axios'
import toastr from 'toastr'
import { FaTimes, FaCheck, FaCheckDouble } from 'react-icons/fa'
import Cari from './Cari'
import moment from 'moment'
import Transaksi from '../transaksi/Tambah'

export class Pasien extends Component {
	constructor(props) {
		super(props)
	
		this.state = {
       showForm: false,
       showFormTransaksi: false,
       showHapus: false,
       isUpdate: false,
       idKunjungan: '',
       namaPasien: '',
       dataKunjungan: []
    }
    this.getFilter({
      tanggalKunjungan: moment().format('YYYY-MM-DD'), 
      namaPasien: localStorage.getItem('klinik_id'), 
      role: localStorage.getItem('klinik_jenis_user')
    })
	}
  
  getData = () => {
    let url;
    if(localStorage.getItem('klinik_jenis_user') === 'pasien'){
      url = `/kunjungan?id_pasien=` + localStorage.getItem('klinik_id') + `&role=pasien`
    }else{
      if(localStorage.getItem('klinik_jenis_user') === 'petugas'){
        url = `/kunjungan?role=petugas`
      }else{
        url = `/kunjungan?role=dokter`
      }
    }

    axios.get(process.env.REACT_APP_API_HOST + url)
      .then( res => {
        this.setState({dataKunjungan: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak adaaaa :(')
      })
  }

  getFilter = (params) => {
    axios.post(process.env.REACT_APP_API_HOST + `/kunjungan/filter`, params)
      .then( res => {
        this.setState({dataKunjungan: res.data})
      })
      .catch( res => {
        toastr.error('Data tanggal '+params.tanggalKunjungan+' tidak ada :(')
      })
  }

  handlePersetujuan = (isTerima, idKunjungan, namaPasien, tanggal, jam) => {
    this.setState({
      idKunjungan: idKunjungan,
      namaPasien: namaPasien,
      tanggal: tanggal,
      jam: jam,
      isTerima: isTerima,
      showPersetujuan: true
    })
  }

  handleShow = () => {
    this.setState({ 
      showPersetujuan: !this.state.showPersetujuan
    })
  }

  handleShowForm = () => {
    this.setState({ showForm: !this.state.showForm })
  }
  
  handleCari = (params) => {
    this.getFilter(params)
  }

  handleShowTransaksi = (idKunjungan, namaPasien) => {
    this.setState({ idKunjungan, namaPasien, showFormTransaksi: !this.state.showFormTransaksi })
  }

  render() {
    const { tanggal, jam, namaPasien, dataKunjungan, idKunjungan, showPersetujuan, showForm, showFormTransaksi, isTerima } = this.state
    return (
      <Fragment>
        <div className="p-3">
          <div className="mb-3">
            <button className="btn btn-primary btn-sm" onClick={() => this.handleShowForm()}>Tambah Data</button>
          </div>
          <div className="card">
            <div className="card-body">
              <Cari 
                handleCari={(e) => this.handleCari(e)}
                handleReset={() => this.getData()}
              />
              <br />
              
              <table className="table table-stripped ">
                <thead className="text-black-50 text-uppercase border-0">
                  <tr>
                    <th className="font-weight-normal border-0">No</th>
                    {
                      (localStorage.getItem('klinik_jenis_user') !== 'pasien') &&
                        <th className="font-weight-normal border-0">Nama Pasien</th>
                    }
                    <th className="font-weight-normal border-0">Tanggal</th>
                    <th className="font-weight-normal border-0">Jam</th>
                    <th className="font-weight-normal border-0">Status</th>
                    {
                      (localStorage.getItem('klinik_jenis_user') !== 'pasien') &&
                        <th className="font-weight-normal border-0 text-center">Aksi</th>                      
                    }
                  </tr>
                </thead>
                <tbody>
                {
                  dataKunjungan.map((data, i) => {
                    return (
                      <tr key={data.id_kunjungan }>
                        <td>{i+1}</td>
                        {
                          (localStorage.getItem('klinik_jenis_user') !== 'pasien') &&
                            <td>{data.pasien}</td>
                        }
                        <td>{data.tgl_kunjungan}</td>
                        <td>{data.jam_kunjungan}</td>
                        <td>{data.status}</td>
                        {
                          (localStorage.getItem('klinik_jenis_user') !== 'pasien') &&
                            <td className="text-center">
                            {
                              data.status === 'Baru' &&
                                <Fragment>
                                  <button className="btn btn-sm btn-default" onClick={() => this.handlePersetujuan(true, data.id_kunjungan, data.pasien, data.tgl_kunjungan, data.jam_kunjungan)}><FaCheck /></button>
                                  <button className="btn btn-sm btn-default" onClick={() => this.handlePersetujuan(false, data.id_kunjungan, data.pasien, data.tgl_kunjungan, data.jam_kunjungan)}><FaTimes /> </button>
                                </Fragment>
                            }
                            {
                              data.status === 'Disetujui' &&
                                <Fragment>
                                  <button className="btn btn-sm btn-default" onClick={() => this.handleShowTransaksi(data.id_kunjungan, data.pasien)}><FaCheckDouble /> </button>
                                </Fragment>
                            }
                            </td>
                        }
                      </tr>
                    )
                  })
                }
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <Transaksi 
          idKunjungan={idKunjungan}
          namaPasien={namaPasien}
          isOpen={showFormTransaksi}
          handleShowTransaksi={() => this.handleShowTransaksi()} 
          getDataKunjungan={() => this.getData()}
        />
        <Persetujuan 
          isOpen={showPersetujuan}
          idKunjungan={idKunjungan}
          nama={namaPasien}
          jam={jam}
          tanggal={tanggal}
          handleShow={() => this.handleShow()} 
          getDataKunjungan={() => this.getData()}
          isTerima={isTerima}
        />
        <Tambah 
          isOpen={showForm}
          handleShow={() => this.handleShowForm()} 
          getDataKunjungan={() => this.getData()}
        />
      </Fragment>
      )
    }
  }
  
  export default Pasien
