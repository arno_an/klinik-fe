import React, { Component, Fragment } from 'react'

export class Cari extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       kategori: ''
    }
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value})
  }
  
  handleSubmit = () => {
    this.props.handleFilter(this.state)
  }

  handlReset = () => {
    this.props.reset()
  }
  
  render() {
    const { kategori } = this.state
    return (
      <Fragment>
        <div className="row">
          <div className="form-group col-sm-3">
            <label>Kategori</label>
            <select name="kategori" className="form-control form-control-sm" onChange={this.handleChange} value={kategori}>
              <option value="">-- Pilih Kategori --</option>
              <option value="Scaling">Scaling (Pembersihan Karang Gigi)</option>
              <option value="Penambalan Gigi">Penambalan Gigi</option>
              <option value="Pencabutan Gigi">Pencabutan Gigi</option>
              <option value="Gigi Palsu">Gigi Palsu</option>
              <option value="Kawat Gigi">Kawat Gigi</option>
              <option value="Pemutihan Gigi">Pemutihan Gigi</option>
            </select>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-3 text-right">
            <button className="btn btn-primary px-4 mr-3 btn-sm" onClick={() => this.handleSubmit()}>Cari</button>
            <button className="btn btn-secondary px-4 btn-sm" onClick={() => this.handlReset()}>Reset</button>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default Cari
