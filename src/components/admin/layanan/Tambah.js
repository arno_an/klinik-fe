import React, { Component, Fragment } from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import axios from 'axios'
import toastr from 'toastr'

class index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      idLayanan: '',
      namaLayanan: '',
      kategori: '',
      hargaLayanan: '',
      keterangan: '',
    }
  }
  
  componentDidMount() {
    // this.props.isUpdate && this.getData(this.props.idPasien)
  }
  
  componentDidUpdate(prevProps, prevState) {
    // console.log(1);
    
    if(prevProps.isUpdate !== this.props.isUpdate && this.props.isUpdate){
      this.getData(this.props.idLayanan)
    }else{
      return false
    }
  }

  getData = (id) => {
    axios.get(process.env.REACT_APP_API_HOST + `/layanan?id=`+ id)
      .then( res => {
        const data = res.data[0]
        this.setState({
          idLayanan: data.id_layanan,
          namaLayanan: data.nama_layanan,
          kategori: data.kategori,
          hargaLayanan: data.harga_layanan,
          keterangan: data.keterangan,
        })
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value})
  }

  handleModal = () => {
    this.props.handleShowTambah()
  }

  handleSubmit = () => {
    if(this.state.password !== this.state.ulangPassword){
      return toastr.warning('Password tidak sama')
    }

    let params = {
      id_layanan: this.state.idLayanan,
      nama_layanan: this.state.namaLayanan,
      kategori: this.state.kategori,
      harga_layanan: this.state.hargaLayanan,
      keterangan: this.state.keterangan,
    }

    this.props.isUpdate ?
      axios.put(process.env.REACT_APP_API_HOST + `/layanan`, params)
        .then(res => {
          toastr.success('Data berhasil diperbarui')
          this.handleModal()
          this.props.getDataLayanan()
        })
    :
      axios.post(process.env.REACT_APP_API_HOST + `/layanan`, params)
        .then(res => {
          toastr.success('Data berhasil disimpan')
          this.handleModal()
          this.props.getDataLayanan()
        })
  }

  render() {
    // console.log(this.props.isUpdate);

    const { idLayanan, namaLayanan, kategori, hargaLayanan, keterangan } = this.state 
    const { isOpen } = this.props
    return (
      <Fragment>
         <Modal isOpen={isOpen}>
          <ModalHeader>Tambah data layanan</ModalHeader>
          <ModalBody>
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="col-12">
                  <input type="hidden" name="idLayanan" value={idLayanan}/>
                  <div className="form-group">
                    <label>Nama Layanan</label>
                    <input type="text" className="form-control" placeholder="Masukan nama layanan" name="namaLayanan" onChange={this.handleChange} maxLength="30" value={namaLayanan}/>
                  </div>
                  <div className="form-group">
                    <label>Kategori</label>
                    <select name="kategori" value={kategori} onChange={this.handleChange} className="form-control">
                      <option value="">Pilih Kategori</option>
                      <option value="Scaling">Scaling (Pembersihan Karang Gigi)</option>
                      <option value="Penambalan Gigi">Penambalan Gigi</option>
                      <option value="Pencabutan Gigi">Pencabutan Gigi</option>
                      <option value="Gigi Palsu">Gigi Palsu</option>
                      <option value="Kawat Gigi">Kawat Gigi</option>
                      <option value="Pemutihan Gigi">Pemutihan Gigi</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <label>Harga Layanan</label>
                    <input type="text" className="form-control" placeholder="Masukan harga layanan" name="hargaLayanan" onChange={this.handleChange} maxLength="15" value={hargaLayanan}/>
                  </div>
                  <div className="form-group">
                    <label>Keterangan</label>
                    <input type="text" className="form-control" placeholder="Masukan keterangan" name="keterangan" onChange={this.handleChange} maxLength="30" value={keterangan}/>
                  </div>
                </div>
              </div>
            </form>
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-primary" onClick={() => this.handleSubmit()}>Submit</button>
            <button className="btn btn-secondary" onClick={() => this.handleModal()}>Cancel</button>
          </ModalFooter>
        </Modal>
      </Fragment>
    )
  }
}

export default index
