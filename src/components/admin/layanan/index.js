import React, { Component, Fragment } from 'react'
import Form from './Tambah'
import Hapus from './Hapus'
import axios from 'axios'
import toastr from 'toastr'
import { FaEdit, FaTrash } from 'react-icons/fa'
import Cari from './Cari';

export class Layanan extends Component {
	constructor(props) {
		super(props)
	
		this.state = {
       showForm: false,
       showHapus: false,
       isUpdate: false,
       idLayanan: '',
       namaLayanan: '',
       dataLayanan: []
    }
    this.getData()    
	}
  
  getData = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/layanan`)
      .then( res => {
        this.setState({dataLayanan: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  getFilter = (params) => {
    axios.post(process.env.REACT_APP_API_HOST + `/layanan/filter`, params)
      .then( res => {
        this.setState({dataLayanan: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  handleUpdate = (idLayanan) => {
    this.setState({
      isUpdate: true,
      idLayanan: idLayanan,
      showForm: true
    })
  }

  handleHapus = (idLayanan, namaLayanan) => {
    this.setState({
      idLayanan: idLayanan,
      namaLayanan: namaLayanan,
      showHapus: true
    })
  }

  handleShowHapus = () => {
    this.setState({ 
      showHapus: !this.state.showHapus
    })
  }

  handleShowTambah = () => {
    this.setState({ 
      isUpdate: false,
      showForm: !this.state.showForm
    })
  }

  handleFilter = (params) => {
    this.getFilter(params)
  }
  
  render() {
    const { namaLayanan, dataLayanan, isUpdate, idLayanan, showForm, showHapus } = this.state
    return (
      <Fragment>
        <div className="p-3">
          <div className="mb-3">
            <button className="btn btn-primary btn-sm" onClick={() => this.handleShowTambah()}>Tambah Data</button>
          </div>
          <div className="card">
            <div className="card-body">
              <Cari 
                handleFilter={(e) => this.handleFilter(e)}
                reset={() => this.getData()}
              />
              <br />
              
              <table className="table table-stripped ">
                <thead className="text-black-50 text-uppercase border-0">
                  <tr>
                    <th className="font-weight-normal border-0">ID</th>
                    <th className="font-weight-normal border-0">Nama Layanan</th>
                    <th className="font-weight-normal border-0">Kategori</th>
                    <th className="font-weight-normal border-0">Harga Layanan</th>
                    <th className="font-weight-normal border-0">Keterangan</th>
                    <th className="font-weight-normal border-0 text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                {
                  dataLayanan.map((data, i) => {
                    return (
                      <tr key={data.id_layanan }>
                        <td>{i+1}</td>
                        <td>{data.nama_layanan}</td>
                        <td>{data.kategori}</td>
                        <td>{data.harga_layanan}</td>
                        <td>{data.keterangan}</td>
                        <td className="text-center">
                          <button className="btn btn-sm btn-default" onClick={() => this.handleUpdate(data.id_layanan)}><FaEdit /></button>
                          <button className="btn btn-sm btn-default" onClick={() => this.handleHapus(data.id_layanan, data.nama_layanan)}><FaTrash/> </button>
                        </td>
                      </tr>
                    )
                  })
                }
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <Form 
          isUpdate={isUpdate} 
          idLayanan={idLayanan}
          isOpen={showForm}
          handleShowTambah={() => this.handleShowTambah()} 
          getDataLayanan={() => this.getData()}
        />
        <Hapus 
          idLayanan={idLayanan}
          nama={namaLayanan}
          handleShowHapus={() => this.handleShowHapus()} 
          isOpen={showHapus}
          getDataLayanan={() => this.getData()}
        />
      </Fragment>
      )
    }
  }
  
  export default Layanan
