import React, { Component } from 'react'

export class Navbar extends Component {
  render() {
    const { location } = this.props
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-primary border-bottom">
        <span className="text-white">{location}</span>
      </nav>
    )
  }
}

export default Navbar
