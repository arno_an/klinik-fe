import React, { Component, Fragment } from 'react'
import 'styles/Admin.css'
import { FaTooth, FaDesktop, FaUsers, FaRegChartBar, FaHandHolding, FaRegCommentDots, FaRegClipboard, FaPowerOff, FaUser } from 'react-icons/fa'
import { NavLink } from "react-router-dom"
import { history } from 'helpers'


export class Sidebar extends Component {
  handleLogout = () => {
    localStorage.clear()
    history.push('/public')
  }
  render() {
    return (
      <div className="bg-white border-right sidebar">
        <div className="sidebar-heading text-primary px-3 py-1 d-flex border-bottom">
          <div className="p-2 mt-1">
            <FaTooth className="text-primary" size={42} />
          </div>
          <div className="p-2">
            <span className="h4">PASO</span>
            <br />
            <small>Klinik Bhakti Mulus</small>
          </div>
        </div>
        <div className="list-group list-group-flush py-3 border-bottom">
          { (localStorage.klinik_jenis_user === 'pasien') && 
            <Fragment>
              <NavLink to={`/admin/kunjungan`} className="list-group-item list-group-item-action pl-3 border-0 text-black-0" activeClassName="active"><FaRegChartBar className="text-primary mr-3" /> Data Kunjungan</NavLink>
              <NavLink to={`/admin/transaksi`} className="list-group-item list-group-item-action pl-3 border-0 text-black-0" activeClassName="active"><FaRegClipboard className="text-primary mr-3" /> Transaksi</NavLink>
            </Fragment>
          }
          { (localStorage.klinik_jenis_user === 'petugas') && 
            <Fragment>
              <NavLink to={`/admin`} exact className="list-group-item list-group-item-action pl-3 border-0 text-black-0" activeClassName="active"><FaDesktop className="text-primary mr-3" /> Dashboard</NavLink>
              <NavLink to={`/admin/kunjungan`} className="list-group-item list-group-item-action pl-3 border-0 text-black-0" activeClassName="active"><FaRegChartBar className="text-primary mr-3" /> Data Kunjungan</NavLink>
              <NavLink to={`/admin/pasien`} className="list-group-item list-group-item-action pl-3 border-0 text-black-0" activeClassName="active"><FaUsers className="text-primary mr-3" /> Data Pasien</NavLink>
              <NavLink to={`/admin/layanan`} className="list-group-item list-group-item-action pl-3 border-0 text-black-0" activeClassName="active"><FaHandHolding className="text-primary mr-3" /> Layanan</NavLink>
              <NavLink to={`/admin/testimoni`} className="list-group-item list-group-item-action pl-3 border-0 text-black-0" activeClassName="active"><FaRegCommentDots className="text-primary mr-3" /> Testimoni</NavLink>
              <NavLink to={`/admin/transaksi`} className="list-group-item list-group-item-action pl-3 border-0 text-black-0" activeClassName="active"><FaRegClipboard className="text-primary mr-3" /> Transaksi</NavLink>
            </Fragment>
          }
          {
            (localStorage.klinik_jenis_user === 'dokter') && 
            <Fragment>
              <NavLink to={`/admin`} exact className="list-group-item list-group-item-action pl-3 border-0 text-black-0" activeClassName="active"><FaDesktop className="text-primary mr-3" /> Dashboard</NavLink>
              <NavLink to={`/admin/transaksi`} className="list-group-item list-group-item-action pl-3 border-0 text-black-0" activeClassName="active"><FaRegClipboard className="text-primary mr-3" /> Transaksi</NavLink>
            </Fragment>
          }
        </div>
        <div>
          {
            (localStorage.klinik_jenis_user === 'petugas') && 
            <Fragment>
              <NavLink to={`/admin/pengguna`} className="list-group-item list-group-item-action pl-3 text-black-0 border-0" activeClassName="active"><FaUsers className="text-muted mr-3" /> Pengguna</NavLink>
            </Fragment>
          }
          {
            (localStorage.klinik_jenis_user === 'pasien') && 
            <Fragment>
              <NavLink to={`/admin/profile`} className="list-group-item list-group-item-action pl-3 text-black-0 border-0" activeClassName="active"><FaUser className="text-muted mr-3" /> Profile</NavLink>
            </Fragment>
          }
          <button onClick={() => this.handleLogout()} className="list-group-item list-group-item-action pl-3 text-black-0 border-0"><FaPowerOff className="text-danger mr-3" /> Logout</button>
        </div>
      </div>
    )
  }
}

export default Sidebar
