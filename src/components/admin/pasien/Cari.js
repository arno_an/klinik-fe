import React, { Component, Fragment } from 'react'

export class Cari extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      namaPasien: '',
      alamat: '',
    }
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value})
  }

  handleSubmit = () => {
    this.props.handleCari(this.state)
  }

  handleReset = () => {
    this.setState({
      namaPasien: '',
      alamat: '',
    })
    this.props.handleReset()
  }
  render() {
    return (
      <Fragment>
        <div className="row">
          <div className="form-group col-sm-3">
            <label>Nama Pasien</label>
            <input type="text" name="namaPasien" className="form-control form-control-sm" onChange={this.handleChange} placeholder="Masukan nama pasien"/>
          </div>
          <div className="form-group col-sm-3">
            <label>Alamat</label>
            <input type="text" name="alamat" className="form-control form-control-sm" onChange={this.handleChange} placeholder="Masukan alamat pasien"/>
          </div>
        </div>
        <div className="row">
          <div className="offset-sm-3 col-sm-3 text-right">
            <button className="btn btn-primary px-4 mr-3 btn-sm" onClick={() => this.handleSubmit()}>Cari</button>
            <button className="btn btn-secondary px-4 btn-sm">Reset</button>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default Cari
