import React, { Component, Fragment } from 'react'
import Registrasi from './Tambah'
import Hapus from './Hapus'
import axios from 'axios'
import toastr from 'toastr'
import { FaEdit, FaTrash } from 'react-icons/fa'
import Cari from './Cari';

export class Pasien extends Component {
	constructor(props) {
		super(props)
	
		this.state = {
       showRegistrasi: false,
       showHapus: false,
       isUpdate: false,
       idPasien: '',
       nama: '',
       dataPasien: []
    }
    this.getData()    
	}
  
  getData = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/pengguna?jenis=pasien`)
      .then( res => {
        this.setState({dataPasien: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  getFilter = (params) => {
    axios.post(process.env.REACT_APP_API_HOST + `/pengguna/filter`, {...params, jenis: 'pasien'})
      .then( res => {
        this.setState({dataPasien: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }
  handleUpdate = (idPasien) => {
    this.setState({
      isUpdate: true,
      idPasien: idPasien,
      showRegistrasi: true
    })
  }

  handleHapus = (idPasien, namaPasien) => {
    this.setState({
      idPasien: idPasien,
      namaPasien: namaPasien,
      showHapus: true
    })
  }

  handleShowHapus = () => {
    this.setState({ 
      showHapus: !this.state.showHapus
    })
  }

  handleShowTambah = () => {
    this.setState({ 
      isUpdate: false,
      showRegistrasi: !this.state.showRegistrasi
    })
  }
  
  handleCari = (params) => {
    this.getFilter(params)
  }
  
  render() {
    const { namaPasien, dataPasien, isUpdate, idPasien, showRegistrasi, showHapus } = this.state
    return (
      <Fragment>
        <div className="p-3">
          <div className="mb-3">
            <button className="btn btn-primary btn-sm" onClick={() => this.handleShowTambah()}>Tambah Data</button>
          </div>
          <div className="card">
            <div className="card-body">
              <Cari 
                handleCari={(e) => this.handleCari(e)}
                handleReset={() => this.getData()}
              />
              <br />
              
              <table className="table table-stripped ">
                <thead className="text-black-50 text-uppercase border-0">
                  <tr>
                    <th className="font-weight-normal border-0">ID</th>
                    <th className="font-weight-normal border-0">Nama</th>
                    <th className="font-weight-normal border-0">No.HP</th>
                    <th className="font-weight-normal border-0">Alamat</th>
                    <th className="font-weight-normal border-0">Total Kunjungan</th>
                    <th className="font-weight-normal border-0 text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                {
                  dataPasien.map((data, i) => {
                    return (
                      <tr key={data.id_pengguna }>
                        <td>{i+1}</td>
                        <td>{data.nama}</td>
                        <td>{data.no_telp}</td>
                        <td>{data.alamat}</td>
                        <td>{data.pekerjaan}</td>
                        <td className="text-center">
                          <button className="btn btn-sm btn-default" onClick={() => this.handleUpdate(data.id_pengguna)}><FaEdit /></button>
                          <button className="btn btn-sm btn-default" onClick={() => this.handleHapus(data.id_pengguna, data.nama)}><FaTrash/> </button>
                        </td>
                      </tr>
                    )
                  })
                }
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <Registrasi 
          isUpdate={isUpdate} 
          idPasien={idPasien}
          isOpen={showRegistrasi}
          handleShowTambah={() => this.handleShowTambah()} 
          getDataPasien={() => this.getData()}
        />
        <Hapus 
          idPasien={idPasien}
          nama={namaPasien}
          handleShowHapus={() => this.handleShowHapus()} 
          isOpen={showHapus}
          getDataPasien={() => this.getData()}
        />
      </Fragment>
      )
    }
  }
  
  export default Pasien
