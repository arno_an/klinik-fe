import React, { Component, Fragment } from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import axios from 'axios'
import toastr from 'toastr'

export class Hapus extends Component {
  handleSubmit = () => {
    axios.delete(process.env.REACT_APP_API_HOST + `/pengguna/` + this.props.idPengguna)
      .then(res => {
        toastr.success('Data berhasil dihapus')
        this.props.getDataPengguna()
        this.handleModal()
      })
  }

  handleModal = () => {
    this.props.handleShowHapus()
  }

  render() {
    const { nama, idPengguna, isOpen } = this.props
    return (
      <Fragment>
         <Modal isOpen={isOpen}>
          <ModalHeader>Hapus Data Pengguna</ModalHeader>
          <ModalBody>
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="col-12">
                  <input type="hidden" name="id_pengguna" value={idPengguna}/>
                  <span>Anda yakin untuk menghapus data <strong>{nama}</strong>?</span>
                </div>
              </div>
            </form>
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-danger" onClick={() => this.handleSubmit()}>Delete</button>
            <button className="btn btn-secondary" onClick={() => this.handleModal()}>Cancel</button>
          </ModalFooter>
        </Modal>
      </Fragment>
    )
  }
}

export default Hapus
