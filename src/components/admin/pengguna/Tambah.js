import React, { Component, Fragment } from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import axios from 'axios'
import toastr from 'toastr'

class index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      idPengguna: '',
      nama: '',
      email: '',
      password: '',
      ulangPassword: '',
      alamat: '',
      noHp: '',
      jenisPengguna: '',
    }
  }
  
  componentDidMount() {
    // this.props.isUpdate && this.getData(this.props.idPengguna)
  }
  
  componentDidUpdate(prevProps, prevState) {
    if(prevProps.isUpdate !== this.props.isUpdate && this.props.isUpdate){
      this.getData(this.props.idPengguna)
    }else{
      return false
    }
  }

  getData = (id) => {
    axios.get(process.env.REACT_APP_API_HOST + `/pengguna?id=`+ id)
      .then( res => {
        const data = res.data[0]
        this.setState({
          idPengguna: data.id_pengguna,
          nama: data.nama,
          email: data.email,
          alamat: data.alamat,
          noHp: data.no_telp,
          jenisPengguna: data.jenis_user,
        })
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value})
  }

  handleModal = () => {
    this.props.handleShowForm()
  }

  handleSubmit = () => {
    if(this.state.password !== this.state.ulangPassword){
      return toastr.warning('Password tidak sama')
    }

    let params = {
      nama: this.state.nama,
      email: this.state.email,
      alamat: this.state.alamat,
      no_telp: this.state.noHp,
      jenis_user: this.state.jenisPengguna,
      id_pengguna: this.state.idPengguna,
    }

    if(this.state.password !== ''){
      params['password'] = this.state.password 
    }

    this.props.isUpdate ?
      axios.put(process.env.REACT_APP_API_HOST + `/pengguna`, params)
        .then(res => {
          toastr.success('Data berhasil diperbarui')
          this.handleModal()
          this.props.getDataPengguna()
        })
    :
      axios.post(process.env.REACT_APP_API_HOST + `/pengguna`, params)
        .then(res => {
          toastr.success('Data berhasil disimpan')
          this.handleModal()
          this.props.getDataPengguna()
        })
  }

  render() {
    // console.log(this.props.isUpdate);

    const { idPengguna, nama, jenisPengguna, email, noHp, password, ulangPassword, alamat } = this.state 
    const { isOpen } = this.props
    return (
      <Fragment>
         <Modal isOpen={isOpen}>
          <ModalHeader>Registrasi Pasien</ModalHeader>
          <ModalBody>
            <form onSubmit={this.handleSubmitLogin}>
              <div className="row">
                <div className="col-12">
                  <input type="hidden" name="idPengguna" value={idPengguna}/>
                  <div className="form-group">
                    <label>Nama</label>
                    <input type="text" className="form-control" placeholder="Masukan nama lengkap" name="nama" onChange={this.handleChange} maxLength="30" value={nama}/>
                  </div>
                  <div className="form-group">
                    <label>Jenis Pengguna</label>
                    <select className="form-control" name="jenisPengguna" onChange={this.handleChange} value={jenisPengguna}>
                      <option value="">Pilih jenis pengguna</option>
                      {
                        ['Dokter', 'Petugas'].map((data, i) => {
                          return <option key={i} value={data.toLowerCase()}>{data}</option>
                        })
                      }
                    </select>
                  </div>
                  <div className="form-group">
                    <label>Email</label>
                    <input type="email" className="form-control" placeholder="Masukan email" name="email" onChange={this.handleChange} maxLength="30" value={email}/>
                  </div>
                  <div className="form-group">
                    <label>No HP</label>
                    <input type="text" className="form-control" placeholder="Masukan no hp" name="noHp" onChange={this.handleChange} maxLength="15" value={noHp}/>
                  </div>
                  <div className="form-group">
                    <label>Alamat</label>
                    <input type="text" className="form-control" placeholder="Masukan alamat lengkap" name="alamat" onChange={this.handleChange} maxLength="50" value={alamat}/>
                  </div>
                  <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Masukan Password" name="password" onChange={this.handleChange} maxLength="30" value={password}/>
                  </div>
                  <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Ulangi Password" name="ulangPassword" onChange={this.handleChange} maxLength="30" value={ulangPassword}/>
                  </div>
                </div>
              </div>
            </form>
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-primary" onClick={() => this.handleSubmit()}>Submit</button>
            <button className="btn btn-secondary" onClick={() => this.handleModal()}>Cancel</button>
          </ModalFooter>
        </Modal>
      </Fragment>
    )
  }
}

export default index
