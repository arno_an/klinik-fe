import React, { Component, Fragment } from 'react'
import Registrasi from './Tambah'
import Hapus from './Hapus'
import axios from 'axios'
import toastr from 'toastr'
import { FaSync, FaEdit, FaTrash } from 'react-icons/fa'

export class Pasien extends Component {
	constructor(props) {
		super(props)
	
		this.state = {
       showForm: false,
       showHapus: false,
       isUpdate: false,
       idPengguna: '',
       namaPasien: '',
       dataPengguna: []
    }
    this.getData()    
	}
  
  getData = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/pengguna?jenis=pengguna`)
      .then( res => {
        this.setState({dataPengguna: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  handleUpdate = (idPengguna) => {
    this.setState({
      isUpdate: true,
      idPengguna: idPengguna,
      showForm: true
    })
  }

  handleHapus = (idPengguna, namaPasien) => {
    this.setState({
      idPengguna: idPengguna,
      namaPasien: namaPasien,
      showHapus: true
    })
  }

  handleShowHapus = () => {
    this.setState({ 
      showHapus: !this.state.showHapus
    })
  }

  handleShowForm = () => {
    this.setState({ 
      isUpdate: false,
      showForm: !this.state.showForm
    })
  }
  
  render() {
    const { namaPasien, dataPengguna, isUpdate, idPengguna, showForm, showHapus } = this.state
    return (
      <Fragment>
        <div className="p-3">
          <div className="mb-3">
            <button className="btn btn-primary btn-sm" onClick={() => this.handleShowForm()}>Tambah Data</button>
          </div>
          <div className="card">
            <div className="card-body">
              <div className="d-flex">
                <input type="text" className="form-control m-auto form-control-sm" placeholder="Search" />
                <button className="btn btn-sm btn-outline-muted float-right m-auto"><FaSync /></button>
              </div>
              <br />
              
              <table className="table table-stripped ">
                <thead className="text-black-50 text-uppercase border-0">
                  <tr>
                    <th className="font-weight-normal border-0">No</th>
                    <th className="font-weight-normal border-0">Nama</th>
                    <th className="font-weight-normal border-0">No.HP</th>
                    <th className="font-weight-normal border-0">Alamat</th>
                    <th className="font-weight-normal border-0">Role</th>
                    <th className="font-weight-normal border-0 text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                {
                  dataPengguna.map((data, i) => {
                    return (
                      <tr key={data.id_pengguna }>
                        <td>{i+1}</td>
                        <td>{data.nama}</td>
                        <td>{data.no_telp}</td>
                        <td>{data.alamat}</td>
                        <td>{data.jenis_user}</td>
                        <td className="text-center">
                          <button className="btn btn-sm btn-default" onClick={() => this.handleUpdate(data.id_pengguna)}><FaEdit /></button>
                          <button className="btn btn-sm btn-default" onClick={() => this.handleHapus(data.id_pengguna, data.nama)}><FaTrash/> </button>
                        </td>
                      </tr>
                    )
                  })
                }
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <Registrasi 
          isUpdate={isUpdate} 
          idPengguna={idPengguna}
          isOpen={showForm}
          handleShowForm={() => this.handleShowForm()} 
          getDataPengguna={() => this.getData()}
        />
        <Hapus 
          idPengguna={idPengguna}
          nama={namaPasien}
          handleShowHapus={() => this.handleShowHapus()} 
          isOpen={showHapus}
          getDataPengguna={() => this.getData()}
        />
      </Fragment>
      )
    }
  }
  
  export default Pasien
