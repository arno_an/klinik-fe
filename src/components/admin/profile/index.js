import React, { Component } from 'react'
import axios from 'axios'
import toastr from 'toastr'

export class index extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       nama: '',
       tgl_lahir: '',
       tgl_daftar: '',
       jenis_kelamin: '',
       alamat: '',
       email: '',
       no_telp: '',
       pekerjaan: '',
       testimoni: '',
       id_pengguna: ''
    }
    this.getData()
  }
  
  getData = () => {
    const url = `/pengguna?id=` + localStorage.getItem('klinik_id')

    axios.get(process.env.REACT_APP_API_HOST + url)
      .then(res => {
        const data = res.data[0]
        this.setState({
          id_pengguna: data.id_pengguna,
          nama: data.nama,
          tgl_lahir: data.tgl_lahir,
          jenis_kelamin: data.jenis_kelamin,
          alamat: data.alamat,
          email: data.email,
          no_telp: data.no_telp,
          pekerjaan: data.pekerjaan,
          testimoni: data.pesan,
          tgl_daftar: data.tgl_daftar,
          ulangi_password: '',
          password: ''
        })
      })
      .catch( res => {
        console.log(res)
      })
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value})
  }

  handleSubmit = () => {
    if(this.state.password !== this.state.ulangi_password){
      return toastr.warning('Password tidak sama')
    }

    const url = `/pengguna/pasien` 
    axios.put(process.env.REACT_APP_API_HOST + url, this.state)
      .then(res => {
        toastr.success('Data berhasil diperbarui')        
      })
      .catch(res => {
        toastr.danger('Data gagal diperbarui')

      })
  }

  render() {
    const { nama, tgl_lahir, tgl_daftar, alamat, jenis_kelamin, email, no_telp, pekerjaan, testimoni, password, ulangi_password } = this.state
    return (
        <div className="container my-5 px-5">
          <div className="text-center h5">Profil Pengguna</div>
          <div className="float-right">
            <span>Tanggal Daftar: {tgl_daftar}</span>
          </div>
          <br/>
          <hr/>
          <div className="content">
            <div className="form-group row">
              <label htmlFor="inputNama" className="col-sm-2 col-form-label">Nama lengkap</label>
              <div className="col-sm-10">
                <input type="text" className="form-control" id="inputNama" name="nama" placeholder="Masukan nama lengkap anda" maxLength="30" defaultValue={nama} onChange={this.handleChange}/>
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="inputTanggalLahir" className="col-sm-2 col-form-label">Tanggal lahir</label>
              <div className="col-sm-10">
                <input type="date" className="form-control" id="inputTanggalLahir" name="tgl_lahir" placeholder="Masukan tanggal lahir" defaultValue={tgl_lahir} onChange={this.handleChange}/>
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="inputTanggalLahir" className="col-sm-2 col-form-label">Jenis Kelamin</label>
              <div className="col-sm-10 form-inline">
                <div className="form-check mr-3">
                  <input className="form-check-input" type="radio" name="jenis_kelamin" id="inputLakilaki" checked={jenis_kelamin === 'laki-laki' ? 'checked' : false} value="laki-laki" onChange={this.handleChange}/>
                  <label className="form-check-label" htmlFor="inputLakilaki">
                    Laki-laki
                  </label>
                </div>
                <div className="form-check">
                  <input className="form-check-input" type="radio" name="jenis_kelamin" id="inputPerempuan" checked={jenis_kelamin === 'perempuan' ? 'checked' : false} value="perempuan" onChange={this.handleChange}/>
                  <label className="form-check-label" htmlFor="inputPerempuan">
                    Perempuan
                  </label>
                </div>
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="inputAlamat" className="col-sm-2 col-form-label">Alamat</label>
              <div className="col-sm-10">
                <input type="text" className="form-control" id="inputAlamat" name="alamat" placeholder="Masukan alamat" maxLength="50" defaultValue={alamat} onChange={this.handleChange}/>
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="inputEmail" className="col-sm-2 col-form-label">Email</label>
              <div className="col-sm-10">
                <input type="text" className="form-control" id="inputEmail" name="email" placeholder="Masukan email" maxLength="30" defaultValue={email} onChange={this.handleChange}/>
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="inputNoTelp" className="col-sm-2 col-form-label">No Telp</label>
              <div className="col-sm-10">
                <input type="text" className="form-control" id="inputNoTelp" name="no_telp" placeholder="Masukan no telp" maxLength="15" defaultValue={no_telp} onChange={this.handleChange}/>
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="inputPekerjaan" className="col-sm-2 col-form-label">Pekerjaan</label>
              <div className="col-sm-10">
                <input type="text" className="form-control" id="inputPekerjaan" name="pekerjaan" placeholder="Masukan pekerjaan" maxLength="30" defaultValue={pekerjaan} onChange={this.handleChange}/>
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="inputPassword" className="col-sm-2 col-form-label">Password</label>
              <div className="col-sm-10">
                <input type="password" className="form-control" id="inputPassword" name="password" placeholder="Masukan password (kosongkan jika tidak ingin mengubah password)" maxLength="30" defaultValue={password} onChange={this.handleChange}/>
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="inputUlanginPassword" className="col-sm-2 col-form-label">Ulangi Password</label>
              <div className="col-sm-10">
                <input type="password" className="form-control" id="inputUlanginPassword" name="ulangi_password" placeholder="Masukan ulangi password" maxLength="30" defaultValue={ulangi_password} onChange={this.handleChange}/>
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="inputTestimoni" className="col-sm-2 col-form-label">Testimoni</label>
              <div className="col-sm-10">
                <textarea className="form-control" id="inputTestimoni" name="testimoni" placeholder="Masukan testimoni" rows="3" value={testimoni} onChange={this.handleChange}></textarea>
              </div>
            </div>

            <div className="row">
              <div className="offset-sm-2 col-sm-10">
                <button type="submit" className="btn btn-primary px-4" onClick={this.handleSubmit}>Simpan</button>

              </div>
            </div>
          </div>
        </div>
      )
  }
}

export default index
