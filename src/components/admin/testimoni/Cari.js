import React, { Component, Fragment } from 'react'
import axios from 'axios'
import toastr from 'toastr'

export class Cari extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       namaPasien: '',
       status: '',
       dataPasien: []
    }
    this.getDataPasien()
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmit = () => {
    this.props.handleFilter({namaPasien: this.state.namaPasien, status: this.state.status})
  }

  handlReset = () => {
    this.setState({ namaPasien: '', status: '' })
    this.props.handleReset()
  }
  
  getDataPasien = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/pengguna?jenis=pasien`)
      .then( res => {
        this.setState({dataPasien: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }


  render() {
    const { namaPasien, status, dataPasien } = this.state
    return (
      <Fragment>
        <div className="row">
          <div className="form-group col-sm-3">
            <label>Nama Pasien</label>
            <select name="namaPasien" className="form-control form-control-sm" onChange={this.handleChange} value={namaPasien}>
              <option value="">-- Pilih Nama Pasien --</option>
              {
                dataPasien.map((data, i) => {
                  return(
                    <option key={i} value={data.id_pengguna}>{data.nama}</option>
                  )
                })
              }
              <option value="Transfer">Transfer</option>
            </select>
          </div>
          <div className="form-group col-sm-3">
            <label>Status</label>
            <select name="status" className="form-control form-control-sm" onChange={this.handleChange} value={status}>
              <option value="">-- Pilih Status --</option>
              <option value="1">Tampil</option>
              <option value="0">Tidak</option>
            </select>
          </div>
        </div>
        <div className="row">
          <div className="offset-sm-3 col-sm-3 text-right">
            <button className="btn btn-primary px-4 mr-3 btn-sm" onClick={() => this.handleSubmit()}>Cari</button>
            <button className="btn btn-secondary px-4 btn-sm" onClick={() => this.handlReset()}>Reset</button>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default Cari
