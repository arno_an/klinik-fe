import React, { Component, Fragment } from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import axios from 'axios'
import toastr from 'toastr'

export class Hapus extends Component {
  handleSubmit = () => {
    axios.delete(process.env.REACT_APP_API_HOST + `/testimoni/`+ this.props.idTestimoni)
      .then(res => {
        toastr.success('Data berhasil dihapus')
        this.handleModal()
        this.props.getDataTestimoni()
      })
      .catch(res => {
        toastr.error('Gagal hapus data')
      })
  }

  handleModal = () => {
    this.props.handleShowHapus()
  }

  render() {
    const { nama, idTestimoni, isOpen } = this.props
    return (
      <Fragment>
         <Modal isOpen={isOpen}>
          <ModalHeader>Hapus Data Testimoni</ModalHeader>
          <ModalBody>
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="col-12">
                  <input type="hidden" name="id_testimoni" value={idTestimoni}/>
                  <span>Anda yakin untuk menghapus data Testimoni dari <strong>{nama}</strong>?</span>
                </div>
              </div>
            </form>
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-danger" onClick={() => this.handleSubmit()}>Delete</button>
            <button className="btn btn-secondary" onClick={() => this.handleModal()}>Cancel</button>
          </ModalFooter>
        </Modal>
      </Fragment>
    )
  }
}

export default Hapus
