import React, { Component, Fragment } from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import axios from 'axios'
import toastr from 'toastr'

class index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      idTestimoni: '',
      idPasien: '',
      namaPasien: '',
      pesan: '',
      isTampil: false,
      dataPasien: []
    }
  }
  
  componentDidMount() {
    // this.props.isUpdate && this.getData(this.props.idPasien)
  }
  
  componentDidUpdate(prevProps, prevState) {
    if(prevProps.isUpdate !== this.props.isUpdate && this.props.isUpdate){
      this.getData(this.props.idTestimoni)

    }else if(prevProps.isOpen !== this.props.isOpen 
      && this.props.isOpen === true 
      && this.props.isUpdate === false)
      {
        this.getDataPasien()
    }else{
      return false
    }

    
  }

  getDataPasien = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/pengguna?jenis=pasien`)
      .then( res => {
        this.setState({dataPasien: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  getData = (id) => {
    axios.get(process.env.REACT_APP_API_HOST + `/testimoni?id=`+ id)
      .then( res => {
        const data = res.data[0]
        
        this.setState({
          idTestimoni: data.id_testimoni,
          // idPasien: data.id_pasien,
          namaPasien: data.nama,
          pesan: data.pesan,
          isTampil: data.is_tampil === '1' ? true : false,
        })
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value})
  }

  handleCheckbox = () => {
    this.setState({isTampil: !this.state.isTampil})
  }

  handleModal = () => {
    this.props.handleShowForm()
  }

  handleSubmit = () => {
    let params = {
      id_testimoni: this.state.idTestimoni,
      id_pasien: localStorage.user_data.is_pengguna, // Dari login
      pesan: this.state.pesan, 
      is_tampil: this.state.isTampil === true ? 1 : 0, // Dibutuhkan untuk admin 
    }

    this.props.isUpdate ?
      axios.put(process.env.REACT_APP_API_HOST + `/testimoni`, params)
        .then(res => {
          toastr.success('Data berhasil diperbarui')
          this.handleModal()
          this.props.getDataTestimoni()
        })
    :
      axios.post(process.env.REACT_APP_API_HOST + `/testimoni`, params)
        .then(res => {
          toastr.success('Data berhasil disimpan')
          this.handleModal()
        })
  }

  render() {
    const { idTestimoni, pesan, namaPasien, isTampil } = this.state 
    const { isOpen, isUpdate } = this.props
    return (
      <Fragment>
         <Modal isOpen={isOpen}>
          <ModalHeader>Form Testimoni</ModalHeader>
          <ModalBody>
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="col-12">
                  {
                    !isUpdate ?
                    <Fragment>
                      <div className="form-group">
                        <label>Pesan</label>
                        <textarea className="form-control" placeholder="Masukan pesan anda" name="pesan" onChange={this.handleChange} cols="10" rows="10" value={pesan} maxLength="255"/>
                      </div>
                    </Fragment>
                    :
                    <Fragment>
                      <input type="hidden" name="idTestimoni" value={idTestimoni}/>
                      <div className="form-group">
                        <label>Nama Pasien: </label>
                        <strong> {namaPasien}</strong>
                      </div>
                      <div className="form-group">
                        <label>Pesan</label>
                        <label>{pesan}</label>
                      </div>
                      <div className="form-check">
                        <label className="form-check-label">
                          <input type="checkbox" className="form-check-input" name="isTampil" onChange={this.handleCheckbox} checked={isTampil} /> Tampil
                        </label>
                      </div>
                    </Fragment>
                  }

                </div>
              </div>
            </form>
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-primary" onClick={() => this.handleSubmit()}>Submit</button>
            <button className="btn btn-secondary" onClick={() => this.handleModal()}>Cancel</button>
          </ModalFooter>
        </Modal>
      </Fragment>
    )
  }
}

export default index
