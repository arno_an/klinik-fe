import React, { Component, Fragment } from 'react'
import Form from './Tambah'
import Hapus from './Hapus'
import axios from 'axios'
import toastr from 'toastr'
import { FaEdit, FaTrash } from 'react-icons/fa'
import Cari from './Cari';

export class Testimoni extends Component {
	constructor(props) {
		super(props)
	
		this.state = {
       showForm: false,
       showHapus: false,
       isUpdate: false,
       idTestimoni: '',
       namaPasien: '',
       dataTestimoni: []
    }
    this.getData()    
	}
  
  getData = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/testimoni`)
      .then( res => {
        this.setState({dataTestimoni: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }
  
  getFilter = (params) => {
    axios.post(process.env.REACT_APP_API_HOST + `/testimoni/filter`, params)
      .then( res => {
        this.setState({dataTestimoni: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  handleUpdate = (idTestimoni) => {
    this.setState({
      isUpdate: true,
      idTestimoni: idTestimoni,
      showForm: true
    })
  }

  handleHapus = (idTestimoni, namaPasien) => {
    this.setState({
      idTestimoni: idTestimoni,
      namaPasien: namaPasien,
      showHapus: true
    })
  }

  handleShowHapus = () => {
    this.setState({ 
      showHapus: !this.state.showHapus
    })
  }

  handleShowForm = () => {
    this.setState({ 
      isUpdate: false,
      showForm: !this.state.showForm
    })
  }

  handleReset = () => {
    this.getData()
  }

  handleFilter = (params) => {
    this.getFilter(params)
  }
  
  render() {
    const { namaPasien, dataTestimoni, idTestimoni, isUpdate, showForm, showHapus } = this.state
    return (
      <Fragment>
        <div className="p-3">
          <div className="mb-3">
            <button className="btn btn-primary btn-sm" onClick={() => this.handleShowForm()}>Tambah Data</button>
          </div>
          <div className="card">
            <div className="card-body">
              <Cari 
                handleFilter={(e) => this.handleFilter(e)}
                handleReset={() =>this.handleReset()}
              />

              <br />
              
              <table className="table table-stripped ">
                <thead className="text-black-50 text-uppercase border-0">
                  <tr>
                    <th className="font-weight-normal border-0">No</th>
                    <th className="font-weight-normal border-0">Nama Pasien</th>
                    <th className="font-weight-normal border-0">Pesan</th>
                    <th className="font-weight-normal border-0">Tanggal Testimoni</th>
                    <th className="font-weight-normal border-0">Tampil</th>
                    <th className="font-weight-normal border-0 text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                {
                  dataTestimoni.map((data, i) => {
                    return (
                      <tr key={data.id_testimoni}>
                        <td>{i+1}</td>
                        <td>{data.nama}</td>
                        <td>{data.pesan}</td>
                        <td>{data.tgl_testimoni}</td>
                        <td>{data.is_tampil === '1' ? "Ya" : "Tidak"}</td>
                        <td className="text-center">
                          <button className="btn btn-sm btn-default" onClick={() => this.handleUpdate(data.id_testimoni)}><FaEdit /></button>
                          <button className="btn btn-sm btn-default" onClick={() => this.handleHapus(data.id_testimoni, data.nama)}><FaTrash/> </button>
                        </td>
                      </tr>
                    )
                  })
                }
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <Form 
          isUpdate={isUpdate} 
          idTestimoni={idTestimoni}
          isOpen={showForm}
          handleShowForm={() => this.handleShowForm()} 
          getDataTestimoni={() => this.getData()}
        />
        <Hapus 
          idTestimoni={idTestimoni}
          nama={namaPasien}
          handleShowHapus={() => this.handleShowHapus()} 
          isOpen={showHapus}
          getDataTestimoni={() => this.getData()}
        />
      </Fragment>
      )
    }
  }
  
  export default Testimoni
