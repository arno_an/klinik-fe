import React, { Component } from 'react'
import DatePicker from "react-datepicker";
import axios from 'axios'
import toastr from 'axios'

import "react-datepicker/dist/react-datepicker.css";

export class Cari extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       tanggalAwal: '',
       tanggalAkhir: '',
       jenisPembayaran: '',
       nominalAwal: '',
       nominalAkhir: '',
       namaPasien: '',
       dataPasien: []
    }
    if(localStorage.getItem('klinik_jenis_user') !== 'pasien'){
      this.getDataPasien()
    }
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value})
  }

  handleSubmit = () => {
    this.props.handleCari(this.state)
  }

  getDataPasien = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/pengguna?jenis=pasien`)
      .then( res => {
        this.setState({dataPasien: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  render() {
    const { tanggalAwal, tanggalAkhir, dataPasien, jenisPembayaran, nominalAwal, nominalAkhir, namaPasien } = this.state

    return (
      <div className="row">
        <div className="col-sm-6 form-row">
          <div className="col-sm-12">
            <label>Tanggal Transaksi</label>
          </div>
          <div className="form-group col-sm-5">
            <DatePicker className="form-control mr-3 form-control-sm" onChange={date => this.setState({tanggalAwal: date}) } selected={tanggalAwal} placeholderText="Pilih tanggal awal"/>
          </div>
          <div className="col-sm-1 text-center">
            <label>s/d</label>
          </div>
          <div className="form-group col-sm-5">
            <DatePicker className="form-control mr-3 form-control-sm" onChange={date => this.setState({tanggalAkhir: date}) } selected={tanggalAkhir} placeholderText="Pilih tanggal akhir"/>
          </div>
        </div>
        <div className="col-sm-6 form-row">
          <div className="form-group col-sm-6">
            <label>Jenis Pembayaran</label>
            <select name="jenisPembayaran" className="form-control form-control-sm" onChange={this.handleChange} value={jenisPembayaran}>
              <option value="">-- Pilih Jenis Pembayaran --</option>
              <option value="Cash">Cash</option>
              <option value="Transfer">Transfer</option>
            </select>
          </div>
        </div>
        
        <div className="col-sm-6 form-row">
          <div className="form-group col-sm-5">
            <label>Nominal Transaksi</label>
            <input type="text" name="nominalAwal" className="form-control mr-3 form-control-sm" placeholder="Dari nominal" onChange={this.handleChange} value={nominalAwal} />
          </div>
          <div className="col-sm-1 text-center">
            <br/>
            <label>s/d</label>
          </div>
          <div className="form-group col-sm-5">
            <label>&nbsp;</label>
            <input type="text" name="nominalAkhir" className="form-control mr-3 form-control-sm" placeholder="Sampai nominal" onChange={this.handleChange} value={nominalAkhir} />
          </div>
        </div>

        { (localStorage.klinik_jenis_user !== 'pasien') &&
        <div className="col-sm-6 form-row">
          <div className="form-group col-sm-6">
            <label>Nama Pasien</label>
            <select name="namaPasien" className="form-control form-control-sm" onChange={this.handleChange} value={namaPasien}>
              <option value="">-- Pilih Nama Pasien --</option>
              { dataPasien.map((data, i) => {
                return (
                  <option key={i} value={data.id_pengguna}>{data.nama}</option>
                )
              })}
            </select>
          </div>
        </div>
        }
      
        <div className="offset-sm-6 col-sm-3 text-right">
          <button className="btn btn-primary px-4 mr-3 btn-sm" onClick={() => this.handleSubmit()}>Cari</button>
          <button className="btn btn-secondary px-4 btn-sm">Reset</button>
        </div>
      </div>
    )
  }
}

export default Cari
