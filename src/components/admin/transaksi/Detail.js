import React, { Component, Fragment } from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import axios from 'axios'
import toastr from 'toastr'

export class Detail extends Component {
	constructor(props) {
		super(props)
	
		this.state = {
			 dataTransaksi: []
		}
	}
  
  componentDidUpdate(prevProps, prevState) {
    if(prevProps.isOpen !== this.props.isOpen && this.props.isOpen){
      this.getData()
    }else{
      return false
    }
  }
  
    
  handleSubmit = () => {
    this.handleModal()
  }

  getData = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/pemeriksaan?id=` +this.props.idPemeriksaan)
      .then( res => {
        this.setState({dataTransaksi: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  handleModal = () => {
    this.props.handleShowDetail()
  }

  render() {
		const { isOpen } = this.props
		const { dataTransaksi } = this.state
    return (
      <Fragment>
         <Modal isOpen={isOpen} size="lg">
          <ModalHeader>Detail Data Transaksi</ModalHeader>
          <ModalBody>
						{

							dataTransaksi.map((data, i) => {
                const detail = data.detail

								return(
                  <Fragment key={i}>
                    <div className="row">
                    { (localStorage.klinik_jenis_user !== 'pasien') &&
                        <div className="col-6 row">
                          <div className="col-6 font-weight-bold">Nama Pasien</div>
                          <div className="col-6">{ data.pasien }</div>
                        </div>
                    }
                      <div className="col-6 row">
                        <div className="col-6 font-weight-bold">Jenis Pembayaran</div>
                        <div className="col-6">{ data.jenis_pembayaran }</div>
                      </div>
                    </div>
                    <div className="row mt-3">
                      <div className="col-6 row">
                        <div className="col-6 font-weight-bold">Tanggal Transaksi</div>
                        <div className="col-6">{ data.tgl_pemeriksaan }</div>
                      </div>
                    </div>
                    <div className="detail mt-3">
                      <table className="table table-hover">
                        <thead className="bg-secondary text-white">
                          <tr>
                            <th>No</th>
                            <th>Tindakan</th>
                            <th>Biaya</th>
                            <th>Keterangan</th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            detail.map((data, i) => {
                              return(
                                <tr key={i}>
                                  <td>{ i+1 }</td>
                                  <td>{data.nama_tindakan}</td>
                                  <td>{data.biaya}</td>
                                  <td>{data.keterangan}</td>
                                </tr>
                              )
                            })
                          }
                          <tr>
                            
                            <td></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </Fragment>
								)
							})
						}
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-secondary" onClick={() => this.handleModal()}>Tutup</button>
          </ModalFooter>
        </Modal>
      </Fragment>
    )
  }
}

export default Detail
