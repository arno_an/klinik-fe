import React, { Component, Fragment } from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import axios from 'axios'
import toastr from 'toastr'

export class Hapus extends Component {
  handleSubmit = () => {
    axios.delete(process.env.REACT_APP_API_HOST + `/pemeriksaan/` +this.props.idTransaksi)
      .then(res => {
        toastr.success('Data berhasil dihapus')
      })
  }

  handleModal = () => {
    this.props.handleShowHapus()
  }

  render() {
    const { tglTransaksi, idPemeriksaan, isOpen } = this.props
    return (
      <Fragment>
         <Modal isOpen={isOpen}>
          <ModalHeader>Hapus Data Transaksi</ModalHeader>
          <ModalBody>
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="col-12">
                  <input type="hidden" name="id_transaksi" value={idPemeriksaan}/>
                  <span>Anda yakin untuk menghapus data <strong>{tglTransaksi}</strong>?</span>
                </div>
              </div>
            </form>
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-danger" onClick={() => this.handleSubmit()}>Delete</button>
            <button className="btn btn-secondary" onClick={() => this.handleModal()}>Cancel</button>
          </ModalFooter>
        </Modal>
      </Fragment>
    )
  }
}

export default Hapus
