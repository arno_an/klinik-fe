import React, { Component, Fragment } from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import DatePicker from 'react-datepicker'
import axios from 'axios'
import toastr from 'toastr'
import "react-datepicker/dist/react-datepicker.css"
import Tindakan from './Tindakan'
import moment from 'moment'

class index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      idDokter: localStorage.klinik_id,
      idTransaksi: '',
      idKunjungan: '',
      namaPasien: '',
      tanggalTransaksi: moment().valueOf(),
      totalBiaya: 0,
      jenisPembayaran: '',
      dataPasien: [],
      dataTindakan: []
    }
  }
  
  componentDidUpdate(prevProps, prevState) {
    if(prevProps !== this.props){
      this.setState({idKunjungan: this.props.idKunjungan, namaPasien: this.props.namaPasien })
    }else{
      return false
    }
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value})
  }

  handleModal = () => {
    this.props.handleShowTransaksi()
  }

  handleSubmit = () => {
    const params = {
      id_dokter: this.state.idDokter,
      id_kunjungan: this.state.idKunjungan,
      tgl_pemeriksaan: this.state.tanggalTransaksi,
      jenis_pembayaran: this.state.jenisPembayaran,
      data_tindakan: this.state.dataTindakan
    }
    axios.post(process.env.REACT_APP_API_HOST + `/pemeriksaan`, params)
      .then(res => {
        toastr.success('Data berhasil disimpan')
        this.handleModal()
        this.props.getDataTransaksi()
      })
  }

  handleDataTransaksi = (dataTindakan, totalBiaya) => {
    this.setState({
      dataTindakan, 
      totalBiaya
    })
  }

  render() {
    const { idDokter, namaPasien, tanggalTransaksi, jenisPembayaran } = this.state 
    const { isOpen } = this.props
    return (
      <Fragment>
         <Modal isOpen={isOpen} size="lg">
          <ModalHeader>Tambah Transaksi</ModalHeader>
          <ModalBody>
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="col-6">
                  <input type="hidden" name="idDokter" value={idDokter}/>
                  <div className="form-group">
                    <label htmlFor="namaPasien">Nama Pasien</label>
                    <input type="text" className="form-control" id="namaPasien" defaultValue={namaPasien} readOnly/>
                  </div>
                  <div className="form-group">
                    <label>Tanggal Pemeriksaan</label>
                    <DatePicker className="form-control mr-3" onChange={date => this.setState({tanggalTransaksi: date}) } selected={tanggalTransaksi} placeholderText="Pilih tanggal transaksi"/>
                  </div>
                </div>
                <div className="col-6">
                  <div className="form-group">
                    <label>Jenis Transaksi</label>
                    <select name="jenisPembayaran" value={jenisPembayaran} onChange={this.handleChange} className="form-control">
                      <option value="">Pilih Jenis Transaksi</option>
                      <option>Cash</option>
                      <option>Debit</option>
                    </select>
                  </div>
                </div>
                <div className="col-12">
                  <Tindakan handleDataTransaksi={(dataTindakan, totalBiaya) => this.handleDataTransaksi(dataTindakan, totalBiaya)}/>
                </div>
              </div>
            </form>
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-primary" onClick={() => this.handleSubmit()}>Submit</button>
            <button className="btn btn-secondary" onClick={() => this.handleModal()}>Cancel</button>
          </ModalFooter>
        </Modal>
      </Fragment>
    )
  }
}

export default index
