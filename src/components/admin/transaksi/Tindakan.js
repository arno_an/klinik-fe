import React, { Component } from 'react'
import Axios from 'axios'

export class Tindakan extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      tindakan: [
        { idTindakan: '', biayaTindakan: '', keterangan: ''  }
      ],
      totalBiaya: 0,
      dataTindakan: []
    }
    this.getTindakan()
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevState.tindakan !== this.state.tindakan){
      this.props.handleDataTransaksi(this.state.tindakan, this.state.totalBiaya)
    }
  }
  

  handleAddTindakan = () => {
    this.setState({ 
      tindakan: this.state.tindakan.concat([
        {
          idTindakan: '',
          biayaTindakan: '',
          keterangan: '' 
        }
      ])
    })
  } 
  
  handleDeleteTindakan = idx => () => {
    const newTindakan = this.state.tindakan.filter((s, sidx) => idx !== sidx ) 
    const totalBiaya = this.countTotalBiaya(newTindakan)

    this.setState({ 
      tindakan: newTindakan,
      totalBiaya
    })
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value})
  }

  handleChangeTindakan = idx => evt => {
    const newTindakan = this.state.tindakan.map((tindakan, sidx) => {
      if(idx !== sidx) return tindakan

      const idTindakan = evt.target.value !== '' ? evt.target.value.split('~')[0] : ''
      const biayaTindakan = evt.target.value !== '' ? evt.target.value.split('~')[1] : ''
      return { 
        ...tindakan, 
        idTindakan,
        biayaTindakan,
      }
    })
    const totalBiaya = this.countTotalBiaya(newTindakan)

    this.setState({ 
      tindakan: newTindakan,
      totalBiaya
    })
  }

  handleChangeKeterangan = idx => evt => {
    const newTindakan = this.state.tindakan.map((tindakan, sidx) => {
      if(idx !== sidx) return tindakan

      return { 
        ...tindakan, 
        [evt.target.name]: evt.target.value,        
      }
    })
   
    this.setState({ 
      tindakan: newTindakan
    })
  }

  countTotalBiaya = (newTindakan) => {
    let totalBiaya = 0
    newTindakan.map((data, i) => {
      return totalBiaya = totalBiaya + (data.biayaTindakan !== "" ? parseInt(data.biayaTindakan) : 0)
    })

    return totalBiaya 
  }

  getTindakan = () => {
    Axios.get(process.env.REACT_APP_API_HOST + `/layanan`)
      .then(res => {
        this.setState({dataTindakan: res.data})
      })
      .catch(res => {
        console.log(res)
      })
  }

  render() {
    const { tindakan, totalBiaya, dataTindakan } = this.state
    return (
      <div className="wrapper-tindakan">
        <hr/>
        <div>
          <button type="button" className="btn btn-sm btn-success" onClick={() => this.handleAddTindakan()} >Tambah Tindakan</button>
        </div>
        <hr/> 
        <table className="table table-stripped">
          <thead>
            <tr>
              <th>Tindakan</th>
              <th>Biaya</th>
              <th>Keterangan</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {
              tindakan.map((data, idx) => {
                return (
                  <tr key={idx}>
                    <td>
                      <select name="idTindakan" id="idTindakan" className="form-control" onChange={this.handleChangeTindakan(idx)}>
                        <option value="">Pilih Tindakan</option>
                        {
                          dataTindakan.map((data, i) => {
                            return(
                              <option key={i} value={data.id_layanan+'~'+data.harga_layanan}>{data.nama_layanan}</option>
                            )
                          })
                        }
                      </select>
                    </td>
                    <td>
                      <input type="text" className="form-control" placeholder="Masukan total biaya" name="biayaTindakan" onChange={this.handleChangeTindakan(idx)} maxLength="12" value={data.biayaTindakan} readOnly/>
                    </td>
                    <td>
                      <input type="text" className="form-control" placeholder="Masukan Keterangan" name="keterangan" onChange={this.handleChangeKeterangan(idx)} maxLength="255" value={data.keterangan}/>
                    </td>
                    <td><button type="button" className="btn btn-danger btn-sm" onClick={this.handleDeleteTindakan(idx)}>Delete</button></td>
                  </tr>
                )
              })
            }
          </tbody>
          <tfoot>
            <tr>
              <td><b>Total Biaya</b></td>
              <td><b>{totalBiaya}</b></td>
            </tr>
          </tfoot>
        </table>
      </div>
    )
  }
}

export default Tindakan
