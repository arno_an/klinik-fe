import React, { Component, Fragment } from 'react'
import Form from './Tambah'
import Hapus from './Hapus'
import Detail from './Detail'
import axios from 'axios'
import toastr from 'toastr'
import { FaFolderOpen } from 'react-icons/fa'
import Cari from './Cari';

export class Transaksi extends Component {
	constructor(props) {
		super(props)
	
		this.state = {
       showDetail: false,
       showForm: false,
       showHapus: false,
       isUpdate: false,
       idPemeriksaan: '',
       tglTransaksi: '',
       dataTransaksi: [],
    }
    this.getData()
	}
  
  getData = () => {
    let url;
    if(localStorage.getItem('klinik_jenis_user') === 'pasien'){
      url = `/pemeriksaan?id_pasien=` + localStorage.getItem('klinik_id')
    }else{
      url = `/pemeriksaan`
    }

    axios.get(process.env.REACT_APP_API_HOST + url)
      .then( res => {
        this.setState({dataTransaksi: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }
  
  getFilter = (params) => {
    axios.post(process.env.REACT_APP_API_HOST + `/pemeriksaan/filter`, params)
      .then( res => {
        this.setState({dataTransaksi: res.data})
      })
      .catch( res => {
        toastr.error('Data tidak ada :(')
      })
  }

  handleUpdate = (idPemeriksaan) => {
    this.setState({
      isUpdate: true,
      idPemeriksaan: idPemeriksaan,
      showForm: true
    })
  }

  handleHapus = (idPemeriksaan, tglTransaksi) => {
    this.setState({
      idPemeriksaan: idPemeriksaan,
      tglTransaksi: tglTransaksi,
      showHapus: true
    })
  }

  handleShowHapus = () => {
    this.setState({ 
      showHapus: !this.state.showHapus
    })
  }

  handleShowTambah = () => {
    this.setState({ 
      isUpdate: false,
      showForm: !this.state.showForm
    })
  }
  
  handleDetail = (idPemeriksaan) => {
    this.setState({ 
      idPemeriksaan: idPemeriksaan,
      showDetail: true
    })
  }

  handleShowDetail = () => {
    this.setState({ 
      showDetail: !this.state.showDetail
    })
  }


  handleCari = (e) => {
    this.getFilter(e)
  }

  render() {
    const { tglTransaksi, dataTransaksi, isUpdate, idPemeriksaan, showForm, showHapus, showDetail } = this.state
    return (
      <Fragment>
        <div className="p-3">
          <div className="mb-3">
            { (localStorage.klinik_jenis_user !== 'pasien') && 
            <Fragment>
                <button className="btn btn-primary btn-sm" onClick={() => this.handleShowTambah()}>Tambah Data</button>
                {/* <button className="btn btn-primary btn-sm float-right" onClick={() => this.handleShowTambah()}>Cetak Laporan</button> */}
            </Fragment>
            }
          </div>
          <div className="card">
            <div className="card-body">
              <Cari 
                handleCari={(e) => this.handleCari(e)}
              />
              <br />
              <table className="table table-stripped ">
                <thead className="text-black-50 text-uppercase border-0 ">
                  <tr>
                    <th className="font-weight-normal border-0">No</th>
                    <th className="font-weight-normal border-0">Tanggal</th>
                    { (localStorage.klinik_jenis_user !== 'pasien') &&
                      <th className="font-weight-normal border-0">Nama Pasien</th>
                    }
                    <th className="font-weight-normal border-0">Total Biaya</th>
                    <th className="font-weight-normal border-0">Jenis Pembayaran</th>
                    <th className="font-weight-normal border-0 text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                {
                  dataTransaksi.map((data, i) => {
                    return (
                      <tr key={i}>
                        <td>{i+1}</td>
                        <td>{data.tgl_pemeriksaan}</td>
                        { (localStorage.klinik_jenis_user !== 'pasien') &&
                          <td>{data.pasien}</td>
                        }
                        <td>{data.total_biaya}</td>
                        <td>{data.jenis_pembayaran}</td>
                        <td className="text-center">
                          { (localStorage.klinik_jenis_user !== 'pasien') &&
                            <Fragment>
                              <button className="btn btn-sm btn-default" onClick={() => this.handleDetail(data.id_pemeriksaan)} title="Lihat detail"><FaFolderOpen/> </button>
                              {/* <button className="btn btn-sm btn-default" onClick={() => this.handleUpdate(data.id_pemeriksaan)} title="Edit"><FaEdit /></button> */}
                              {/* <button className="btn btn-sm btn-default" onClick={() => this.handleHapus(data.id_pemeriksaan, data.tgl_pemeriksaan)} title="Hapus"><FaTrash/> </button> */}
                            </Fragment>
                          }
                          { (localStorage.klinik_jenis_user === 'pasien') &&
                            <button className="btn btn-sm btn-default" onClick={() => this.handleDetail(data.id_pemeriksaan)}><FaFolderOpen/> </button>
                          }
                        </td>
                      </tr>
                    )
                  })
                }
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <Form 
          isUpdate={isUpdate} 
          idPemeriksaan={idPemeriksaan}
          isOpen={showForm}
          handleShowTambah={() => this.handleShowTambah()} 
          getDataTransaksi={() => this.getData()}
        />
        <Hapus 
          idPemeriksaan={idPemeriksaan}
          tglTransaksi={tglTransaksi}
          handleShowHapus={() => this.handleShowHapus()} 
          isOpen={showHapus}
        />
        <Detail 
          idPemeriksaan={idPemeriksaan}
          handleShowDetail={() => this.handleShowDetail()} 
          isOpen={showDetail}
        />
      </Fragment>
      )
    }
  }
  
  export default Transaksi
