import React, { Component, Fragment } from 'react'
import 'styles/Public.css'
import banner from 'assets/image/banner-1.png'
import Registrasi from 'components/admin/pasien/Tambah'
// import { carousel } from 'reactstrap'

class Header extends Component{
	constructor(props) {
		super(props)
	
		this.state = {
			 showRegistrasi: false
		}
	}
	
  handleShowTambah = () => {
    this.setState({showRegistrasi: !this.state.showRegistrasi})
	}
	
	render(){
    const { showRegistrasi } = this.state

		return(
			<div id="carouselBanner" className="carousel slide" data-ride="carousel">
				<Fragment>
					<ol className="carousel-indicators">
						<li data-target="#carouselBanner" data-slide-to="0" className="active"></li>
						<li data-target="#carouselBanner" data-slide-to="1"></li>
						<li data-target="#carouselBanner" data-slide-to="2"></li>
					</ol>
					<div className="carousel-inner">
						<Fragment>
							<div className="carousel-caption d-none d-md-block text-dark">
								<h3 className="font-weight-bold">Waktu Operasional</h3>
								<p className="font-italic">Senin s/d Minggu, Pukul 08.00 - 20.00</p>
								<button className="btn btn-primary shadow-sm pl-4 pr-4" onClick={() => this.handleShowTambah()}>Registrasi Online</button>
							</div>
							<div className="carousel-item active">
								<img src={banner} className="d-block w-100" alt="..."></img>
							</div>
							<div className="carousel-item">
								<img src={banner} className="d-block w-100" alt="..."></img>
							</div>
							<div className="carousel-item">
								<img src={banner} className="d-block w-100" alt="..."></img>
							</div>
						</Fragment>
					</div>
					<a className="carousel-control-prev" href="#carouselBanner" role="button" data-slide="prev">
						<span className="carousel-control-prev-icon" aria-hidden="true"></span>
						<span className="sr-only">Previous</span>
					</a>
					<a className="carousel-control-next" href="#carouselBanner" role="button" data-slide="next">
						<span className="carousel-control-next-icon" aria-hidden="true"></span>
						<span className="sr-only">Next</span>
					</a>
				</Fragment>
				<Registrasi 
					isUpdate={false} 
					handleShowTambah={() => this.handleShowTambah()} 
					isOpen={showRegistrasi}
					getDataPasien={() => console.log("ok")}
				/>
			</div>
			)
		}
	}
	
	export default Header