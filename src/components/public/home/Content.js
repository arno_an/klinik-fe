import React, { Component, Fragment } from 'react' 
import bgBlue from 'assets/image/bg-blue.png'
import bgCenter from 'assets/image/bg-center.png'
import iconSecure from 'assets/icon/ic-secure.png'
import iconOk from 'assets/icon/ic-ok.png'
import iconSmile from 'assets/icon/ic-smile.png'
import product1 from 'assets/image/product-1.png'
import product2 from 'assets/image/product-2.png'
import product3 from 'assets/image/product-3.png'
import informasiUmum from 'assets/image/informasi-umum.png'

class Content extends Component{
	render(){
		return(
			<Fragment>
				<div className="container-fluid" style={{background: `url(${bgCenter})`}}>
					<div className="row">
						<div className="col-12"  style={{backgroundImage: `url(${bgBlue})`, backgroundSize: 'cover'}}>
							<div className="mt-5 mb-4 text-white text-center">
								<span className="font-weight-normal h4 ">Kenapa </span>
								<span className="font-weight-bold h4">Klinik Bhakti Mulus?</span>
								<br />
								<br />
								<p>Mungkin sebagian dari Anda masih bingung memilih dokter gigi pribadi yang tepat</p>
							</div>
						</div>
					</div>
					<div className="container">
						<div className="row text-center mt-5 pb-5 pt-2 content-why-us">
							<div className="col-4">
								<img src={iconSecure}  alt="Keamanan terjaga" className="mb-3"></img>
								<br />
								<span className="h5 text-primary">Keamanan data pasien</span>
								<p className="mt-3">Login terpusat untuk setiap transaksi demi keamanan data pasien</p>
							</div>
							<div className="col-4">
								<img src={iconOk} alt="Biaya Terjangkau" className="mb-3"></img>
								<br />
								<span className="h5 text-primary">Biaya Terjangkau</span>
								<p className="mt-3">Biaya terjangkau, peralatan yang steril, lengkap dan terbaru</p>
							</div>
							<div className="col-4">
								<img src={iconSmile} alt="Ramah" className="mb-3"></img>
								<br />
								<span className="h5 text-primary">Ramah</span>
								<p className="mt-3">Tim dokter dan petugas yang ramah dan kekeluargaan</p>
							</div>
						</div>
					</div>
				</div>
			
				<div className="container-fluid text-center pt-5 pb-5" style={{background: 'rgb(232, 238, 246)'}}>
					<h3 className="text-primary">Jenis Perawatan</h3>
					<br />
					<div className="container">
						<div className="row">
							<div className="col-4 pl-4 pr-4">
								<span><i>Ortodhontic</i></span>
								<br />
								<b>Pemasangan Kawat Gigi</b>
								<img src={product1} alt="Ortodhontic" className="mt-3 w-100"></img>
							</div>
							<div className="col-4 pl-4 pr-4">
								<span><i>Scaling</i></span>
								<br />
								<b>Pembersihan Karang Gigi</b>
								<img src={product2} alt="Scaling" className="mt-3 w-100"></img>
							</div>
							<div className="col-4 pl-4 pr-4">
								<span><i>Prosthodontic</i></span>
								<br />
								<b>Pemasangan Gigi Palsu</b>
								<img src={product3} alt="Prosthodontic" className="mt-3 w-100"></img>
							</div>
						</div>
					</div>
				</div>
				<div className="container text-center pt-5 pb-2">
					<h3 className="text-primary mb-4">Informasi Umum</h3>
					<img src={informasiUmum} alt="Informasi Umum"></img>
					<p className="text-muted mt-4 word-space">Klinik Bhakti Mulus Dental Care berdiri sejak tahun 2011. Bhakti Mulus Dental Care adalah klinik gigi spesialis terpercaya di indonesia. Beberapa layanan yang ditangani oleh klini ini antara lain adalah: Konsultasi gigi, Perawatan gigi, Pengobatan gigi, Pemeriksaan gigi, Pembentukan gigi kembali, Pembuatan gigi tiruan dan lain lain..</p>
					
					{/* <div style={{height: '250px'}}></div> */}
				</div>
			</Fragment>
			)
		}
	}
	
	export default Content