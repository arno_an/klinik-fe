import React, { Component, Fragment } from 'react'
import 'styles/Public.css'


class Footer extends Component{
    render(){
        return(
            <Fragment>
                <div className="container-fluid text-center text-white bg-primary p-2 word-space">
                    <small>© Copyright 2019 - Klinik Bhakti Mulus</small>
                </div>
            </Fragment>
        )
    }
}

export default Footer