import React, { Component, Fragment } from 'react'
import iconIg from 'assets/icon/ic-ig.png'
import iconCall from 'assets/icon/ic-call.png'
import iconMail from 'assets/icon/ic-mail.png'

import 'styles/Public.css'


class Header extends Component{
	render(){
		return(
			<Fragment>
				<div className="header-nav position-absolute pl-3 w-100 p-2">
					<ul className="list-inline">
						<li className="list-inline-item text-white mr-4">
							<img src={iconIg} alt="ig"></img> drg.heni
						</li>
						<li className="list-inline-item text-white mr-4">
							<img src={iconCall} alt="telp"></img> +62 89 658 398 442
						</li>
						<li className="list-inline-item text-white mr-4">
							<img src={iconMail} alt="email"></img> bhaktimulus@gmail.com
						</li>
					</ul>
				</div>
			</Fragment>
			)
		}
	}
	
	export default Header