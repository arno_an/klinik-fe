import React, { Component, Fragment } from 'react'
import Header from './Header'
import Banner from './Banner'
import Content from './Content'
import Footer from './Footer'
import Testimoni from '../testimoni'
import axios from 'axios'

class index extends Component {
	constructor(props) {
		super(props)
	
		this.state = {
			 items: []
		}
		this.getData()
	}
	
	getData = () => {
		axios.get(process.env.REACT_APP_API_HOST + `/testimoni?is_tampil=1`)
			.then( res => {
				let arrItem = []
				res.data.map((item) => {
					arrItem.push({
						altText: item.pesan,
						caption: item.nama
					})

					this.setState({
						items: arrItem
					})
					return true
				})
			})
	}
	
	render() {
		return (
			<Fragment>
				<Header />
				<Banner />
				<Content />
				<Testimoni items={this.state.items}/>
				<Footer />
			</Fragment>
		)
	}
}

export default index