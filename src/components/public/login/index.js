import React, { Component } from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import axios from 'axios'
import toastr from "toastr"
import { history } from 'helpers'

class index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
    }
  }
  
  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value})
  }

  handleModal = () => {
    this.props.handleLoginClick()
  }

  handleSubmitLogin = () => {
    const params = { 
      email: this.state.email,
      password: this.state.password
    }

    axios.post(process.env.REACT_APP_API_HOST + `/pengguna/login`, params)
      .then( res => {
        localStorage.setItem("klinik_id", res.data[0].id_pengguna)
        localStorage.setItem("klinik_nama", res.data[0].nama)
        localStorage.setItem("klinik_email", res.data[0].email)
        localStorage.setItem("klinik_jenis_user", res.data[0].jenis_user)

        if(localStorage.getItem('klinik_jenis_user') === 'pasien'){
          history.push('/admin/kunjungan')
        }else{
          history.push('/admin')
        }
        
        toastr.success('Selamat datang di aplikasi klinik bhakti mulus ')
      })
      .catch( res => {
        toastr.error('Username / Password tidak sesuai ')
      })
    }

  render() {
    const { showLogin } = this.props
    return (
      <div>
         <Modal isOpen={showLogin}>
          <ModalHeader>Login</ModalHeader>
          <ModalBody>
          <form onSubmit={this.handleSubmitLogin}>
            <div className="row">
              <div className="col-12">
                <div className="form-group">
                  <label>Email</label>
                  <input type="email" className="form-control" placeholder="Masukan email anda" name="email" onChange={this.handleChange} />
                  {/* <small className="form-text text-muted">We'll never share your email with anyone else.</small> */}
                </div>
                <div className="form-group">
                  <label>Password</label>
                  <input type="password" className="form-control" placeholder="Password" name="password" onChange={this.handleChange} />
                </div>
              </div>
            </div>
          </form>
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-primary" onClick={() => this.handleSubmitLogin()}>Submit</button>
            <button className="btn btn-secondary" onClick={() => this.handleModal()}>Cancel</button>
          </ModalFooter>
        </Modal>
      </div>
    )
  }
}

export default index
