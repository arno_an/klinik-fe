import React, { Component } from 'react'
import axios from 'axios'
import toastr from 'toastr'

export class index extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       dataLayanan: []
    }
    this.getLayanan()
  }
  
  getLayanan = () => {
    axios.get(process.env.REACT_APP_API_HOST + `/layanan`)
      .then( res => {
        const data = res.data
        let arrCat = []
        let arrDetail = []

        data.map((val, i) => {
          if(arrCat.indexOf(val.kategori) === -1){
            arrCat.push(val.kategori)

            arrDetail.push({
              nama: val.kategori,
              detail: [
                {
                  nama: val.nama_layanan,
                  harga: val.harga_layanan
                }
              ]
            })
          }else{
            arrDetail[arrCat.indexOf(val.kategori)].detail.push({
              nama: val.nama_layanan,
              harga: val.harga_layanan
            })
          }
          return true
        })

        this.setState({dataLayanan: arrDetail})
      })
      .catch( err => {
        // toastr.error('Data eweh ada :(')
        toastr.error(err)
      })
  }

  render() {
    const { dataLayanan } = this.state
    return (
      <div className="container pt-4">
        <div className="row">
          <div className="col-12 h5 mt-5 text-center mb-5 h5">
            ~ Pricelist Layanan ~
          </div>
          { dataLayanan.map((data, i) => {
            return (
              <div className="col-6 px-5" key={i}>
                <div className="text-center font-weight-bold mb-2 text-primary">{data.nama}</div>
                <table className="table">
                  <tbody>
                    { data.detail.map((detail, i) => {
                      return (
                        <tr key={i}>
                          <td className="td-nama"><div>{detail.nama}</div></td>
                          <td className="text-right font-weight-bold">Rp. {detail.harga}</td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
              </div>
            )
          }) }
        </div>
      </div>
    )
  }
}

export default index
