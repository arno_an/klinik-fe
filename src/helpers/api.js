import toastr from 'toastr'
import axios from 'axios'

// Get data
export function GET_DATA(url, params){
  return axios.get(url, params)
    .catch(function(error){
      toastr.error(error)
    })
  }
  
export function POST(url, params){
  return axios.post(url, params)
    .catch(function(error){
      toastr.error(error)
    })
}
  
export function PUT(url, params){
  return axios.put(url, params)
    .catch(function(error){
      toastr.error(error)
    })
}

export function DELETE(url){
  return axios.delete(url)
    .catch(function(error){
      toastr.error(error)
    })
    .done(function(data){
      toastr.success(data)
    })
}